#!/usr/bin/env python2
import sys, re
import ascii_tree

tsv_columns = 'show turn_id word_id text disf pos en chunk dep gov text_id lemma morpho speaker'.split()

class color:
    RED = '\033[31m'
    BLUE = '\033[34m'
    GREEN = '\033[32m'
    END = '\033[0m'

# valid queries:
# (text='a'|ne="b"),pos=v/dep=SUJ
class Query:
    def __init__(self, text):
        self.tree = self.compile(text)

    def compile(self, text):
        try:
            p, tree = self.path_operator(text, 0)
            #print p, len(text), tree
            if p == len(text):
                return tree
            else:
                raise Exception('expecting more input', p)
        except Exception as e:
            if len(e.args) != 2:
                raise e
            message, p = e.args
            print >>sys.stderr, 'Error parsing query:', message
            print >>sys.stderr, text
            print >>sys.stderr, (' ' * p) + '^'
        return None

    ### simple grammar for queries ###
    # path_operator: logic_operator [/\<>] path_operator | logic_operator(0)
    # logic_opeartor(0): logic_opeartor(1) "|" logic_operator(0) | logic_operator(1)
    # logic_opeartor(1): parentheses "," logic_operator(1) | parentheses
    # parentheses: "(" logic_operator(0) ")" | constraint
    # constraint: "." | name comparator value
    # name: valid column name
    # comparator: [= != ~ !~]
    # value: "string" | 'string' | string without special characters

    path_operators = '/\\<>'
    def path_operator(self, text, p):
        p, first = self.logic_operator(text, p)
        if p < len(text) and text[p] in Query.path_operators:
            operator = text[p]
            p, second = self.path_operator(text, p + 1)
            return p, [operator, first, second]
        return p, first

    logic_operators = '/\\|,'
    def logic_operator(self, text, p, operator=0):
        #print 'op:', text, p, operator
        if operator < len(Query.logic_operators) - 1:
            p, first = self.logic_operator(text, p, operator + 1)
        else:
            p, first = self.parentheses(text, p)
        if p < len(text) and text[p] == Query.logic_operators[operator]:
            p, second = self.logic_operator(text, p + 1, operator)
            return p, [Query.logic_operators[operator], first, second]
        return p, first

    def parentheses(self, text, p):
        #print 'par:', text, p
        if p < len(text) and text[p] == '(':
            p, tree = self.logic_operator(text, p + 1)
            if p < len(text) and text[p] == ')':
                return p + 1, tree
            raise Exception('mismatched parentheses', p)
        p, tree = self.constraint(text, p)
        return p, tree
    
    def constraint(self, text, p):
        if text[p:].startswith('.'):
            return p + 1, ['.']
        else:
            p, name = self.name(text, p)
            p, comparator = self.comparator(text, p)
            p, value = self.value(text, p)
        return p, [comparator, name, value]

    def name(self, text, p):
        for column in tsv_columns:
            if text[p:].startswith(column):
                return p + len(column), column
        raise Exception('invalid column name', p)

    def comparator(self, text, p):
        for comparator in ['=', '!=', '~', '!~']:
            if text[p:].startswith(comparator):
                return p + len(comparator), comparator
        raise Exception('invalid comparator', p)

    value_stoppers = '()' + logic_operators + path_operators
    def value(self, text, p):
        start = p
        if text[start] in '"\'':
            p += 1
            while p < len(text) and text[p] != text[start]:
                if text[p] == '\\':
                    p += 2
                p += 1
            if p == len(text):
                raise Exception('mismatched string delimiter', p)
            return p + 1, text[start + 1: p]
        else:
            while p < len(text) and text[p] not in Query.value_stoppers:
                p += 1
            return p, text[start: p]

class Word:
    def __init__(self, tokens):
        for i, column in enumerate(tsv_columns):
            setattr(self, column, tokens[i])
        self.parent = None
        self.next = None
        self.previous = None
        self.turn = None
        self.children = []
        self.color = None

    def matches(self, query):
        if query[0] == '.':
            return set([self])
        elif query[0] == '!=':
            if getattr(self, query[1]) != query[2]:
                return set([self])
        elif query[0] == '=':
            if getattr(self, query[1]) == query[2]:
                return set([self])
        elif query[0] == '~':
            if re.search(query[2], getattr(self, query[1])):
                return set([self])
        elif query[0] == '!~':
            if not re.search(query[2], getattr(self, query[1])):
                return set([self])
        elif query[0] == '|':
            return self.matches(query[1]) | self.matches(query[2])
        elif query[0] == ',':
            return self.matches(query[1]) & self.matches(query[2])
        elif query[0] == '/':
            if self.matches(query[1]):
                output = set()
                for child in self.children:
                    output.update(child.matches(query[2]))
                return output
        elif query[0] == '\\':
            if self.parent != None and self.matches(query[1]):
                return self.parent.matches(query[2])
        elif query[0] == '>':
            if self.next != None and self.matches(query[1]):
                return self.next.matches(query[2])
        elif query[0] == '<':
            if self.previous != None and self.matches(query[1]):
                return self.previous.matches(query[2])
        else:
            raise Exception('unsupported match type', query[0])
        return set()

    def __repr__(self):
        return '\t'.join([getattr(self, x) for x in tsv_columns])

class Turn:
    def __init__(self, filename, words):
        for i in range(len(words)):
            if i > 0:
                words[i].previous = words[i - 1]
            if i < len(words) - 1:
                words[i].next = words[i + 1]
            gov = int(words[i].gov) - 1
            if gov >= 0:
                words[i].parent = words[gov]
                words[gov].children.append(words[i])
            words[i].turn = self
        self.words = words
        self.turn_id = words[0].turn_id
        self.show = words[0].show

    def matches(self, query):
        output = set()
        for word in self.words:
            output.update(word.matches(query))
        return output

    def __repr__(self):
        return '\n'.join([repr(x) for x in self.words]) + '\n'

class Tsv:
    def __init__(self, filename):
        turns = []
        with open(filename) as fp:
            sentence = []
            for line in fp:
                tokens = line.strip().split()
                if len(tokens) == 0:
                    turns.append(Turn(filename, [Word(x) for x in sentence]))
                    sentence = []
                else:
                    sentence.append(tokens)
        self.turns = turns
        self.filename = filename
        self.show = self.turns[0].show

class Index:
    def __init__(self):
        self.files = []

    def add(self, tsv):
        self.files.append(tsv)

    def search(self, query):
        query = Query(query).tree
        output = []
        if query != None:
            for tsv in self.files:
                for turn in tsv.turns:
                    for word in turn.matches(query):
                        output.append(word)
        return output

    def view(self, show, turn_id=None):
        for tsv in self.files:
            if tsv.show == show:
                for turn in tsv.turns:
                    if turn_id == None or turn_id == turn.turn_id:
                        print turn.show, turn.turn_id
                        ascii_tree.print_tree(turn.words, lambda x: [y.decode('utf8') for y in [x.text, x.pos, x.disf, x.en, x.chunk]], lambda x: int(x.gov), lambda x: x.dep)
                        #print turn
    def list(self):
        for tsv in self.files:
            print tsv.show, '(%d turns)' % len(tsv.turns)

def help():
    print 'Commands:'
    print '  help                           show this message.'
    print '  list                           list name of shows.'                     
    print '  view <show-name> [turn-id]     display the content of a specific show/turn.'
    print '  search <query>                 search tsv contents using the query language.'
    print '  quit                           end session.'
    print
    print 'Query language using dependency tree paths:'
    print
    print 'A path is a sequence of assertions on word attributes separated by path operators (a/b/c/d).'
    print 'Path operators are word/child, word\\parent, word>next, word<previous.'
    print 'Assertions can be attribute=value, attribute!=value, attribute~value, attribute!~value (regular expressions).'
    print 'Valid attribute names are: ' + ' '.join(tsv_columns)
    print 'Assertions can be combined with "|" (or) and "," (and) and grouped with parentheses.'
    print 'Values can be defined as quote-enclosed strings. "." is a special assertion that is always true.'
    print
    print 'Examples:'
    print '  lemma=aller/dep=SUJ            subject of verb "aller".'
    print '  pos=v/pos=vinf                 argument of control/raising verbs.'
    print '  pos=vinf\pos=v                 control/raising verbs.'
    print '  lemma=de>.\.                   parents of words after "de".'

if __name__ == '__main__':
    index = Index()
    print >>sys.stderr, 'loading %d files' % len(sys.argv[1:])
    for tsv in sys.argv[1:]:
        index.add(Tsv(tsv))
    
    import os, stat
    mode = os.fstat(0).st_mode
    interactive = not (stat.S_ISFIFO(mode) or stat.S_ISREG(mode))
    if interactive:
        import readline
        histfile = os.path.join(os.path.expanduser("~"), ".tsv_explorer_history")
        try:
            readline.read_history_file(histfile)
        except IOError:
            pass
        import atexit
        atexit.register(readline.write_history_file, histfile)

    while True:
        try:
            if interactive:
                query = raw_input('query> ')
            else:
                query = sys.stdin.readline()
        except EOFError, KeyboardInterrupt:
            break 
        if not query:
            break
        query = query.strip()
        if query == 'help':
            help()
        elif query.startswith('view '):
            print '\t'.join(tsv_columns)
            tokens = query.split()
            if len(tokens) == 2:
                index.view(tokens[1])
            else:
                index.view(tokens[1], tokens[2])

        elif query == 'list':
            index.list()
        elif query == 'quit':
            break
        elif query.startswith('search '):
            found = index.search(query[7:].strip())
            turns = {}
            for word in found:
                turns[word.turn_id] = word.turn
                word.color = color.RED
            for turn in sorted(turns.values(), key=lambda x: x.show + '.%010d' % int(x.turn_id)):
                words = []
                for word in turn.words:
                    if word.color != None:
                        words.append(color.RED + word.text + color.END)
                    else:
                        words.append(word.text)
                print turn.show, turn.turn_id, ' '.join(words)
            for word in found:
                word.color = None
            print '%d hits' % len(found)
        else:
            help()

