# -*- coding: utf8 -*-
import sys, os

class Word:
    def __init__(self, index, fields):
        self.index = index
        self.fields = fields
        self.left = []
        self.right = []
        self.length = 0
        self.position = 0

    def add_arc(self, arc):
        if ar.is_root or arc.min == self.index:
            self.right.append(arc)
        elif arc.max == self.index:
            self.left.append(arc)

    def compute_length(self):
        self.length = max([len(x) for x in self.fields] + [len(self.left) + len(self.right)]) + 1
        for arc in self.right:
            if arc.is_root:
                self.length = max([self.length, len(arc.label)])

    def compute_position(self, x):
        self.left = sorted(self.left, key=lambda arc: arc.height)
        self.right = sorted(self.right, key=lambda arc: -arc.height)
        self.num = len(self.left) + len(self.right)
        self.center = x + self.length / 2

        offset = - self.num / 2
        for arc in self.left:
            arc.max_offset = offset
            offset += 1
        for arc in self.right:
            arc.min_offset = offset
            offset += 1

        return x + self.length

class Arc:
    def __init__(self, source, target, label):
        self.source = int(source)
        self.target = int(target)
        self.label = label
        self.above = []
        self.height = 1
        self.min = min([self.source, self.target])
        self.max = max([self.source, self.target])
        self.min_offset = 0
        self.max_offset = 0
        self.is_root = (target == -1)
        if self.is_root:
            self.min = self.source
            self.max = self.source
        self.length = len(self.label) + 1

    def add_to_words(self, words):
        if self.min != -1: words[self.min].right.append(self)
        if not self.is_root:
            words[self.max].left.append(self)

    def compute_depth(self):
        value = 0
        for above in self.above:
            value = max([value, above.compute_depth()])
        self.height = value + 1
        return self.height

    def compute_above(self, arcs):
        self.above = []
        for arc in arcs:
            if self != arc and not self.is_root and not arc.is_root:
                if self.min <= arc.min and self.max >= arc.min \
                        or self.min <= arc.max and self.max >= arc.max:
                    if self.max - self.min > arc.max - arc.min:
                        self.above.append(arc)
                    #elif self.min == arc.min and self.max == arc.max:
                    #    pass # this is the case where both arcs are identical

    def make_space(self, words):
        start = words[self.min].center + self.min_offset + 1
        end = words[self.max].center + self.max_offset + 1
        if self.is_root:
            for word in words[self.source + 1:]:
                for arc in word.right:
                    if arc.is_root:
                        first_finish = words[self.min].center - self.length / 2 + 1 + len(self.label) + 1
                        second_start = word.center - arc.length / 2
                        difference = second_start - first_finish - 1
                        if difference < 0:
                            for other in words[word.index:]:
                                other.center -= difference
                        break
        else:
            current_length = end - start
            if current_length < self.length:
                difference = self.length - current_length
                for word in words[self.max:]:
                    word.center += difference

    def compute_position(self, words):
        self.start = words[self.min].center + self.min_offset + 1
        self.end = words[self.max].center + self.max_offset + 1

#line_clip = int(os.popen('tput cols').readline().strip())

class Line:
    def __init__(self):
        self.content = []

    def set(self, position, characters):
        if len(self.content) < position + len(characters):
            self.content += list(u' ' * (position + len(characters) - len(self.content)))
        for i, c in enumerate(characters):
            if (c == u'│' and self.content[position + i] == u'─') or (c == u'─' and self.content[position + i] == u'│'):
                c = u'┼'
            self.content[position + i] = c

    def __repr__(self):
        global line_clip
        output = []
        for x in self.content:
            if not isinstance(x, unicode):
                output.append(x.decode('utf8'))
            else:
                output.append(x)
        return u''.join(output[:]).encode('utf8')

def print_tree(sentence, field_getter, governor_getter, label_getter):
    #print sentence[0][0], sentence[0][1] + ':'
    print

    words = []
    arcs = []
    for i, tokens in enumerate(sentence):
        words.append(Word(i, field_getter(tokens)))
        arcs.append(Arc(i, governor_getter(tokens) - 1, label_getter(tokens)))

    for arc in arcs:
        arc.compute_above(arcs)

    # compute maximum height
    max_height = 0
    for arc in arcs:
        arc.compute_depth()
        arc.add_to_words(words)
        if arc.height > max_height: max_height = arc.height

    max_height += 1 # acount for roots
    for arc in arcs:
        if arc.is_root:
            arc.height = max_height

    position = 0
    for word in words:
        word.compute_length()
        position = word.compute_position(position)

    # make space for arcs which have a long label
    arcs = sorted(arcs, key=lambda arc: arc.max - arc.min)
    for arc in arcs:
        arc.make_space(words)

    for arc in arcs:
        arc.compute_position(words)
    
    arcs = sorted(arcs, key=lambda arc: -arc.height)

    while max_height > 0:
        line = Line()
        for arc in arcs:
            if arc.height > max_height:
                line.set(arc.start, u'│')
                if not arc.is_root:
                    line.set(arc.end, u'│')
        for arc in arcs:
            if arc.height == max_height:
                if not arc.is_root:
                    # horizontal line and label
                    line.set(arc.start + 1, u'─' * (arc.end - arc.start))
                    offset = (arc.start + arc.end) / 2 - len(arc.label) / 2
                    line.set(offset, arc.label)
                    line.set(arc.start, u'┌')
                    line.set(arc.end, u'┐')
                else:
                    # label for root
                    offset = arc.start - arc.length / 2 + 1
                    line.set(offset, arc.label)
        print line
        max_height -= 1

    # add arrow heads
    line = Line()
    for arc in arcs:
        if arc.source == arc.min:
            line.set(arc.start, u'│')
            if arc.target != -1:
                line.set(arc.end, u'▾')
        else:
            line.set(arc.end, u'│')
            if arc.target != -1:
                line.set(arc.start, u'▾')
    print line

    # add words
    for i in range(len(words[0].fields)):
        line = Line()
        for word in words:
            line.set(word.center - word.length / 2 + 1, word.fields[i])
        #if i == 0:
        #    print Color.YELLOW + repr(line) + Color.END
        #else:
        print line
    print
    #print

if __name__ == '__main__':
    sentence = []
    for line in sys.stdin:
        tokens = line.decode('utf8').strip().split()
        if len(tokens) == 0:
            print_tree(sentence, 2, 9)
            sentence = []
        else:
            sentence.append(tokens)

